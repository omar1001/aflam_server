var mysql = require('mysql');

function get_data(
				packages,
				movies, 
				clips, 
				clips_choices, 
				packages_clips, 
				complete_callback, 
				error_callback) {

    console.log('try to connect');
	var connection = require('./util').connect();
	console.log('connected');
	
	var respond = {};
	get_packages();
	console.log('packages called');
	function get_packages() {
		console.log('pack'+packages);
		connection.query('SELECT id AS i, points AS p, clips AS c, color AS co, number AS n FROM movies_quiz.packages WHERE published = 1 AND number > ?', [packages], function(err, results) {
		    if(err) {
		        error_callback(err);
				connection.end();
		    }
		    else {
		    	respond.p = results; //packages
				get_movies();
		    }
		});
	}

	function get_movies() {
	    connection.query('SELECT id AS i, name AS m, number AS n FROM movies_quiz.movies WHERE number > ?', [movies], function(err, results) {
		    if(err) {
		        error_callback(err);
				connection.end();
		    }
		    else {
		    	respond.m = results;//movies
				get_clips();
		    }
		});
	}
	
	function get_clips() {
	    connection.query('SELECT id AS i, movie_id AS mi, clip AS c, number AS n FROM movies_quiz.clips WHERE number > ?', [clips], function(err, results) {
		    if(err) {
		        error_callback(err);
				connection.end();
		    }
		    else {
				respond.c = results;//clips
				get_clips_choices();
		    }
		});
	} 

	function get_clips_choices() {
	    connection.query('SELECT id AS i, movie_id AS mi, clip_id AS ci, number AS n FROM movies_quiz.movies_clips_choices WHERE number > ?', [clips_choices], function(err, results) {
		    if(err) {
		        error_callback(err);
				connection.end();
		    }
		    else {
		    	respond.cc = results;//clips choices
				get_packages_clips();
		    }
		    
		});
	} 	
	
	function get_packages_clips() {
	    connection.query('SELECT id AS i, package_id AS pi, clip_id AS ci, number AS n FROM movies_quiz.packages_clips WHERE number > ?', [packages_clips], function(err, results) {
		    if(err) {
		        error_callback(err);
		    }
		    else {
				respond.pc = results;//packages clips
				complete_callback(respond);
		    }
		    connection.end();
		});
	} 
}

module.exports.get_data = get_data;
