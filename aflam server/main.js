var express = require('express');


var app = express();


app.get('/check_data', function (req, res) {
	console.log('check_data called');
	require('./check').get_data(
		req.param('p', '-1'),//packages
		req.param('m', '-1'),//movies
		req.param('c', '-1'),//clips
		req.param('cc', '-1'),//clips choices
		req.param('pc', '-1'),//packages clips
		function (respond) {
			res.send(respond);
		},
		function (error) {
			//PRODUCTION TODO: HANDLE ERROR.
			throw error;
		}
	)
});

var server = app.listen(3000, function () {
   	var host = server.address().address
   	var port = server.address().port

   	console.log("Example app listening at http://%s:%s", host, port)
});
